package com.urunsatisi.urun.app.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "kategori")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Kategori {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer ustKategori;
    private String adi;
    private String aktif;

    @ManyToMany(mappedBy = "kategoriler")
    private Set<Urun> urunler;
}

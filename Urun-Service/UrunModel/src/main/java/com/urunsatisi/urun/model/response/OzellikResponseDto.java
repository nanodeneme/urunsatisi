package com.urunsatisi.urun.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OzellikResponseDto {

    private Integer id;
    private String type;
    private String value;
    private Integer kategoriId;
}

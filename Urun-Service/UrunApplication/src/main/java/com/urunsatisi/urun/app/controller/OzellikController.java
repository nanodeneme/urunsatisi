package com.urunsatisi.urun.app.controller;

import com.urunsatisi.urun.model.request.OzellikAddRequestDto;
import com.urunsatisi.urun.model.response.OzellikResponseDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("/v1/ozellik")
public interface OzellikController {

    @PostMapping("/add")
    ResponseEntity<OzellikResponseDto> saveOzellik(@RequestBody @Valid OzellikAddRequestDto ozellikAddRequestDto);

    @GetMapping("/ozellikler")
    ResponseEntity<List<OzellikResponseDto>> getOzellikler();

}

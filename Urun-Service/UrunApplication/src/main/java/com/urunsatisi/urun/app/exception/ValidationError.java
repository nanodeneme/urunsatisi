package com.urunsatisi.urun.app.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ValidationError extends USSubError {
    private String object;
    private String field;
    private Object rejectedValue;
    private String message;
}

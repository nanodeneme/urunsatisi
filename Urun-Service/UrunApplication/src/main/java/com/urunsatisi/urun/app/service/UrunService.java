package com.urunsatisi.urun.app.service;

import com.urunsatisi.urun.model.request.UrunRequestDto;
import com.urunsatisi.urun.model.response.UrunResponseDto;

public interface UrunService {

    UrunResponseDto getUrunById(UrunRequestDto urunRequestDto);

    UrunResponseDto addUrun(UrunRequestDto urunRequestDto);

}

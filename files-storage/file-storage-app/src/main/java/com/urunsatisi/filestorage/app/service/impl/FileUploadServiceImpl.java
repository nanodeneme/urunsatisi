package com.urunsatisi.filestorage.app.service.impl;

import com.urunsatisi.filestorage.app.service.FileUploadService;
import com.urunsatisi.filestorage.app.util.FileUploadUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class FileUploadServiceImpl implements FileUploadService {

    @Override
    public String saveFile(List<MultipartFile> multipartFiles) {
        StringBuilder builder = new StringBuilder();
        multipartFiles.forEach(f->{
            builder.append(FileUploadUtil.saveFile(f));
            builder.append("\n");
        });
        return builder.toString();
    }

    @Override
    public byte[] getFile(String fileName) {
        return FileUploadUtil.getFile(fileName);
    }
}

package com.urunsatisi.urun.app.service;

import com.urunsatisi.urun.model.request.OzellikAddRequestDto;
import com.urunsatisi.urun.model.response.OzellikResponseDto;

import java.util.List;
import java.util.Set;

public interface OzellikService {

    OzellikResponseDto saveOzellik(OzellikAddRequestDto ozellikAddRequestDto);

    List<OzellikResponseDto> getOzelliklkerByKategoriId(Set<Integer> ozellikIds, Set<Integer> kategoriId);

    List<OzellikResponseDto> getAllOzellik();

}

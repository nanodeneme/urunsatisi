package com.urunsatisi.urun.app.service;

import com.urunsatisi.urun.model.request.BirimAddDto;
import com.urunsatisi.urun.model.response.BirimResponseDto;

import java.util.List;

public interface BirimService {


    BirimResponseDto addBirim(BirimAddDto birimAddDto);

    List<BirimResponseDto> getAllBirim();

    BirimResponseDto getBirimById(Integer id);

}

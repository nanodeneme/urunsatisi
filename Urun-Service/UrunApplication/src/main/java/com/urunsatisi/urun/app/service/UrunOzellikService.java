package com.urunsatisi.urun.app.service;

import com.urunsatisi.urun.app.entity.UrunOzellik;
import com.urunsatisi.urun.model.response.UrunOzellilkResponseDto;

import java.util.List;

public interface UrunOzellikService {

    UrunOzellilkResponseDto saveUrunOzellik(List<UrunOzellik> urunOzellik);

}

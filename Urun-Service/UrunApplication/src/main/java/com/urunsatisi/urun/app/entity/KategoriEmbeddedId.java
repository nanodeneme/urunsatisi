package com.urunsatisi.urun.app.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KategoriEmbeddedId implements Serializable {
    private static final long serialVersionUID = 993303710808964092L;
    private Integer id;
    private Integer ustId;
    private String kategori;
}

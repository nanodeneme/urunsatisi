package com.urunsatisi.urun.app.controller.impl;

import com.urunsatisi.urun.app.controller.KategoriController;
import com.urunsatisi.urun.model.response.KategoriDto;
import com.urunsatisi.urun.app.service.KategoriService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class KategoriControllerImpl implements KategoriController {

    private final KategoriService kategoriService;

    @Override
    public ResponseEntity<KategoriDto> kategoriEkle(@Valid @RequestBody KategoriDto requestDto) {
        return ResponseEntity.ok(kategoriService.saveKategori(requestDto));
    }

    @Override
    public ResponseEntity<List<KategoriDto>> getTumKategoriler() {
        return ResponseEntity.ok(kategoriService.getTumKategoriler());
    }

    @Override
    public ResponseEntity<KategoriDto> getKategoriById(Integer id) {
        return ResponseEntity.ok(kategoriService.getKategoriById(id));
    }

    @Override
    public ResponseEntity<KategoriDto> deleteKategoriById(Integer id) {
        return ResponseEntity.ok(kategoriService.deleteKategoriById(id));
    }

}

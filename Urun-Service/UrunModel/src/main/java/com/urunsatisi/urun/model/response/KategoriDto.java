package com.urunsatisi.urun.model.response;

import com.urunsatisi.urun.model.validator.EvetHayirValidation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KategoriDto {

    private int id;
    @NotNull(message = "Kategori Adı Null Olamaz.")
    @NotEmpty(message = "Kategori Adı Boş Olamaz")
    private String adi;
    @Min(value = 0, message = "Üst Kategori 0'dan Küçük Olamaz")
    private int ustKategori;
    @NotNull(message = "Kategori Aktiflik Durumu Null Olamaz")
    @NotEmpty(message = "Kategori Aktiflik Durumu Boş Olamaz")
    //@Size(min = 1, max = 1, message = "Kategori Aktiflik Alanı E veya H Olabilir.")
    //@Pattern(regexp = "^(E|H)$", message = "Kategori Aktiflik Alanı E veya H Olabilir.")
    @EvetHayirValidation(message = "Kategori Aktiflik Alanı E veya H Olabilir.")
    private String aktif;
}

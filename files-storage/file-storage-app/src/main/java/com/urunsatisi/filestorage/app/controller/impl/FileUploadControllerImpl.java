package com.urunsatisi.filestorage.app.controller.impl;

import com.urunsatisi.filestorage.app.controller.FileUploadController;
import com.urunsatisi.filestorage.app.service.FileUploadService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
public class FileUploadControllerImpl implements FileUploadController {

    private final FileUploadService fileUploadService;

    public FileUploadControllerImpl(FileUploadService fileUploadService) {
        this.fileUploadService = fileUploadService;
    }

    @Override
    public ResponseEntity<String> fileUpload(List<MultipartFile> multipartFiles) {
        return ResponseEntity.ok(fileUploadService.saveFile(multipartFiles));
    }

    @Override
    public ResponseEntity<byte[]> download(String fileName) {
        return ResponseEntity.ok(fileUploadService.getFile(fileName));
    }
}

package com.urunsatisi.urun.app.controller.impl;

import com.urunsatisi.urun.app.controller.UrunController;
import com.urunsatisi.urun.app.service.UrunService;
import com.urunsatisi.urun.model.request.UrunRequestDto;
import com.urunsatisi.urun.model.response.UrunResponseDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/urun")
public class UrunControllerImpl implements UrunController {

    private final UrunService urunService;

    public UrunControllerImpl(UrunService urunService) {
        this.urunService = urunService;
    }

    @Override
    public ResponseEntity<UrunResponseDto> getUrunById(UrunRequestDto urunRequestDt) {
        return ResponseEntity.ok(urunService.getUrunById(urunRequestDt));
    }

    @Override
    public ResponseEntity<UrunResponseDto> addUrun(UrunRequestDto urunRequestDto) {
        return ResponseEntity.ok(urunService.addUrun(urunRequestDto));
    }
}

package com.urunsatisi.urun.app.repository;

import com.urunsatisi.urun.app.entity.Ozellik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface OzellikRepository extends JpaRepository<Ozellik, Integer> {

    List<Ozellik> findByKategoriId(Integer kategoriId);

    List<Ozellik> findByIdInAndKategoriIdIn(Set<Integer> ozellikIds, Set<Integer> kategoriId);
}

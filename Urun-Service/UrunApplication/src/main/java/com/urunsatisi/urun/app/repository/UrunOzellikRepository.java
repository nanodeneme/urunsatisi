package com.urunsatisi.urun.app.repository;

import com.urunsatisi.urun.app.entity.UrunOzellik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UrunOzellikRepository extends JpaRepository<UrunOzellik, Long> {
}

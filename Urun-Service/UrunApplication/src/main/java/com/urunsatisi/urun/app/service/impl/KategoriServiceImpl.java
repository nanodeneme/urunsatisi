package com.urunsatisi.urun.app.service.impl;

import com.urunsatisi.urun.app.entity.Kategori;
import com.urunsatisi.urun.app.exception.RecordNotFoundException;
import com.urunsatisi.urun.app.exception.UsRuntimeException;
import com.urunsatisi.urun.app.mapper.USMapper;
import com.urunsatisi.urun.model.response.KategoriDto;
import com.urunsatisi.urun.app.repository.KategoriRepository;
import com.urunsatisi.urun.app.service.KategoriService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;


@Service
@RequiredArgsConstructor
public class KategoriServiceImpl implements KategoriService {

    private final KategoriRepository kategoriRepository;


    @Override
    public KategoriDto saveKategori(KategoriDto requestDto) {
        Kategori kategori = kategoriRepository.findByAdiAndUstKategori(requestDto.getAdi(), requestDto.getUstKategori());
        if(kategori != null){
            throw new UsRuntimeException("Böyle bir kategori daha önce eklendi.");
        }
        if(requestDto.getUstKategori() != 0){
            Optional<Kategori> ustKategori = kategoriRepository.findById(requestDto.getUstKategori());
            if (ustKategori.isEmpty()) {
                throw new RecordNotFoundException("Girmiş Olduğunuz Üst Kategori Bulunamadı.");
            }
        }
        Kategori savedKategori = kategoriRepository.save(USMapper.map(requestDto, Kategori.class));
        return USMapper.map(savedKategori, KategoriDto.class);
    }

    @Override
    public List<KategoriDto> getTumKategoriler() {
        List<Kategori> entities = kategoriRepository.findAll();
        if(entities.isEmpty()){
            throw new RecordNotFoundException("Kategoriler bulunamadı.");
        }
        return USMapper.mapAll(entities, KategoriDto.class);
    }

    @Override
    public KategoriDto getKategoriById(Integer id) {
        Kategori kategori = kategoriRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException(String.format("Kategori bulunmadı. Sorgulanan Id: %s", id)));
        return USMapper.map(kategori, KategoriDto.class);
    }

    @Override
    public KategoriDto deleteKategoriById(Integer id) {
        Kategori kategori = kategoriRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException(String.format("Kategori bulunmadı. Sorgulanan Id: %s", id)));
        kategoriRepository.deleteById(id);
        return USMapper.map(kategori, KategoriDto.class);
    }

    @Override
    public List<KategoriDto> getKategoriDtosByIds(Set<Integer> kategoriIds) {
        List<Kategori> kategoris = kategoriRepository.findByIdIn(kategoriIds);
        if(kategoris.isEmpty()){
            throw new RecordNotFoundException(String.format("Belirtilen Kategori Id'leri ile kategoriler bulunamadı. %s", kategoris));
        }
        return USMapper.mapAll(kategoris, KategoriDto.class);
    }


}

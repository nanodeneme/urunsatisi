package com.urunsatisi.urun.model.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = EvetHayirValidator.class)
public @interface EvetHayirValidation {
    String message() default "E, H olabilir";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}

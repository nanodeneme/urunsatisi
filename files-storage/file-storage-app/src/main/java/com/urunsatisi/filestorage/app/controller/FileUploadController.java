package com.urunsatisi.filestorage.app.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RequestMapping("/v1/file")
public interface FileUploadController {

    @PostMapping(value = "/upload", consumes = "multipart/form-data")
    ResponseEntity<String> fileUpload(@RequestParam("image[]") List<MultipartFile> multipartFiles);

    @GetMapping(value = "/download", produces = MediaType.IMAGE_JPEG_VALUE)
    ResponseEntity<byte[]> download(@RequestParam("fileName") String fileName);

}

package com.urunsatisi.urun.app.controller;

import com.urunsatisi.urun.model.request.BirimAddDto;
import com.urunsatisi.urun.model.response.BirimResponseDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("/v1/birim")
public interface BirimController {

    @PostMapping("/add")
    ResponseEntity<BirimResponseDto> addBirim(@Valid @RequestBody BirimAddDto birimAddDto);

    @GetMapping("/birims")
    ResponseEntity<List<BirimResponseDto>> getBirims();

}

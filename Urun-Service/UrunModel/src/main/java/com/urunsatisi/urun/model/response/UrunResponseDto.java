package com.urunsatisi.urun.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UrunResponseDto {
    private Long id;

    private String name;

    private String aktif;

    private String aciklama;

    private List<OzellikResponseDto> urunResponseDtos;
}

package com.urunsatisi.urun.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UrunRequestDto {

    private Long id;

    @NotEmpty
    private String name;

    @Pattern(regexp = "^(E|H)$", message = "Kategori Aktiflik Alanı E veya H Olabilir.")
    private String aktif;

    @NotEmpty
    private String aciklama;

    @NotEmpty(message = "Kategori alanı boş olamaz.")
    private Set<@NotNull(message = "En Az 1 Kategori Girmelisiniz.") Integer> kategoriIds;

    @NotEmpty(message = "Ozellik alanı boş olamaz.")
    private Set<@NotNull(message = "En Az 1 Ozellik Girmelisiniz.") Integer> ozelliks;

    @NotNull
    @Min(value = 1)
    private Integer birimId;

}

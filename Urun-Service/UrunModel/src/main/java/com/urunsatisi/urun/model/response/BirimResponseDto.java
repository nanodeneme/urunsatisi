package com.urunsatisi.urun.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BirimResponseDto {

    private Integer id;

    private String birimAdi;

    private String aciklama;

}

package com.urunsatisi.urun.model.response;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UrunOzellilkResponseDto {

    private Long id;
    private OzellikResponseDto ozellik;
    private UrunResponseDto urun;

}

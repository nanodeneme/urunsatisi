package com.urunsatisi.urun.app.controller.impl;

import com.urunsatisi.urun.app.controller.OzellikController;
import com.urunsatisi.urun.model.request.OzellikAddRequestDto;
import com.urunsatisi.urun.model.response.OzellikResponseDto;
import com.urunsatisi.urun.app.service.OzellikService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OzellikControllerImpl implements OzellikController {

    private final OzellikService ozellikService;

    public OzellikControllerImpl(OzellikService ozellikService) {
        this.ozellikService = ozellikService;
    }

    @Override
    public ResponseEntity<OzellikResponseDto> saveOzellik(OzellikAddRequestDto ozellikAddRequestDto) {
        return ResponseEntity.ok(ozellikService.saveOzellik(ozellikAddRequestDto));
    }

    @Override
    public ResponseEntity<List<OzellikResponseDto>> getOzellikler() {
        return ResponseEntity.ok(ozellikService.getAllOzellik());
    }
}

package com.urunsatisi.urun.model.validator;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EvetHayirValidator implements ConstraintValidator<EvetHayirValidation, String> {

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        Pattern pattern = Pattern.compile("^(E|H)$");
        Matcher matcher = pattern.matcher(s);
        return matcher.find();
    }
}

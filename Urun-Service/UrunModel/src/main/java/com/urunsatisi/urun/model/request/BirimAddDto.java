package com.urunsatisi.urun.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BirimAddDto {

    @NotEmpty(message = "Birim Adı Boş Olamaz")
    private String birimAdi;

    private String aciklama;

}

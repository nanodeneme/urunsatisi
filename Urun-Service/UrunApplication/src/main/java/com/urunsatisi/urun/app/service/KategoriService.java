package com.urunsatisi.urun.app.service;

import com.urunsatisi.urun.model.response.KategoriDto;

import java.util.List;
import java.util.Set;

public interface KategoriService {

    KategoriDto saveKategori(KategoriDto requestDto);

    List<KategoriDto> getTumKategoriler();

    KategoriDto getKategoriById(Integer id);

    KategoriDto deleteKategoriById(Integer id);

    List<KategoriDto> getKategoriDtosByIds(Set<Integer> kategoriIds);

}

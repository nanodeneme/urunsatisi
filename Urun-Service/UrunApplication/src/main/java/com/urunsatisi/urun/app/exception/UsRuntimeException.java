package com.urunsatisi.urun.app.exception;

public class UsRuntimeException extends RuntimeException{

    public UsRuntimeException(String message){
        super(message);
    }
}

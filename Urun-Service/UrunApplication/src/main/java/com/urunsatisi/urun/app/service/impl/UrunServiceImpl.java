package com.urunsatisi.urun.app.service.impl;

import com.urunsatisi.urun.app.entity.*;
import com.urunsatisi.urun.app.mapper.USMapper;
import com.urunsatisi.urun.app.service.*;
import com.urunsatisi.urun.model.response.BirimResponseDto;
import com.urunsatisi.urun.model.response.KategoriDto;
import com.urunsatisi.urun.app.repository.UrunRepository;
import com.urunsatisi.urun.model.request.UrunRequestDto;
import com.urunsatisi.urun.model.response.OzellikResponseDto;
import com.urunsatisi.urun.model.response.UrunResponseDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UrunServiceImpl implements UrunService {

    private final UrunRepository urunRepository;
    private final KategoriService kategoriService;
    private final OzellikService ozellikService;
    private final UrunOzellikService urunOzellikService;
    private final BirimService birimService;

    public UrunServiceImpl(UrunRepository urunRepository, KategoriService kategoriService, OzellikService ozellikService, UrunOzellikService urunOzellikService, BirimService birimService) {
        this.urunRepository = urunRepository;
        this.kategoriService = kategoriService;
        this.ozellikService = ozellikService;
        this.urunOzellikService = urunOzellikService;
        this.birimService = birimService;
    }

    @Override
    public UrunResponseDto getUrunById(UrunRequestDto urunRequestDto) {
        Urun urun = urunRepository.findById(urunRequestDto.getId())
                .orElseThrow(NoSuchElementException::new);
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(urun, UrunResponseDto.class);
    }

    @Override
    public UrunResponseDto addUrun(UrunRequestDto urunRequestDto) {
        Set<KategoriDto> kategoriler = new HashSet<>(kategoriService.getKategoriDtosByIds(urunRequestDto.getKategoriIds()));
        Set<Kategori> kaydedilecekKategoriler = kategoriler.stream().map(k -> USMapper.map(k, Kategori.class))
                .collect(Collectors.toSet());

        BirimResponseDto birimResponseDto = birimService.getBirimById(urunRequestDto.getBirimId());

        Urun eklenecekUrun = USMapper.map(urunRequestDto, Urun.class);
        eklenecekUrun.setBirim(USMapper.map(birimResponseDto, Birim.class));
        eklenecekUrun.setKategoriler(kaydedilecekKategoriler);
        Urun eklenenUrun = urunRepository.save(eklenecekUrun);


        //Bu kısımda
        //select * from ozellik where ozellik_id in(1,2,3,x,y,z) and kategori_id in(4,5,6)
        List<OzellikResponseDto> ozelliks = ozellikService.getOzelliklkerByKategoriId(urunRequestDto.getOzelliks(), urunRequestDto.getKategoriIds());
        List<UrunOzellik> urunOzelliks = ozelliks.stream().map(o -> convertToUrunOzellik(o, eklenenUrun ))
                .collect(Collectors.toList());
        urunOzellikService.saveUrunOzellik(urunOzelliks);
        return USMapper.map(eklenenUrun, UrunResponseDto.class);
    }

    private UrunOzellik convertToUrunOzellik(OzellikResponseDto o, Urun eklenenUrun) {
        UrunOzellik urunOzellik = new UrunOzellik();
        urunOzellik.setOzellik(USMapper.map(o, Ozellik.class));
        urunOzellik.setUrun(eklenenUrun);
        return urunOzellik;
    }
}

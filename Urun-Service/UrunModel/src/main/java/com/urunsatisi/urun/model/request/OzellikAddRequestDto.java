package com.urunsatisi.urun.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OzellikAddRequestDto {

    @NotEmpty
    private String type;
    @NotEmpty
    private String value;
    @NotNull
    private Integer kategoriId;
}

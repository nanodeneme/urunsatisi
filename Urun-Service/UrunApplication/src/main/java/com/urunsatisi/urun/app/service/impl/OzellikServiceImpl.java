package com.urunsatisi.urun.app.service.impl;

import com.urunsatisi.urun.app.entity.Ozellik;
import com.urunsatisi.urun.app.exception.RecordNotFoundException;
import com.urunsatisi.urun.app.mapper.USMapper;
import com.urunsatisi.urun.model.request.OzellikAddRequestDto;
import com.urunsatisi.urun.model.response.OzellikResponseDto;
import com.urunsatisi.urun.app.repository.OzellikRepository;
import com.urunsatisi.urun.app.service.KategoriService;
import com.urunsatisi.urun.app.service.OzellikService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class OzellikServiceImpl implements OzellikService {

    private final OzellikRepository ozellikRepository;
    private final KategoriService kategoriService;

    public OzellikServiceImpl(OzellikRepository ozellikRepository, KategoriService kategoriService) {
        this.ozellikRepository = ozellikRepository;
        this.kategoriService = kategoriService;
    }

    @Override
    public OzellikResponseDto saveOzellik(OzellikAddRequestDto ozellikAddRequestDto) {

        //Kategori servisinde böyle bir kateogri var mı yok mu
        //kontrlü amacıyl yazdık.
        kategoriService.getKategoriById(ozellikAddRequestDto.getKategoriId());

        Ozellik savedOzellik = ozellikRepository.save(USMapper.map(ozellikAddRequestDto, Ozellik.class));
        return USMapper.map(savedOzellik, OzellikResponseDto.class);
    }

    @Override
    public List<OzellikResponseDto> getOzelliklkerByKategoriId(Set<Integer> ozellikIds,Set<Integer> kategoriId) {
        List<Ozellik> ozelliks = ozellikRepository.findByIdInAndKategoriIdIn(ozellikIds, kategoriId);
        if(ozelliks.isEmpty()){
            throw new RecordNotFoundException(String.format("Urun Ozellikleri ve Kategori Id'ye ait özellikler bulunamadı. Kategori Id: %s, Ozellik Ids: %s",
                    kategoriId,ozellikIds.toString()));
        }
        return USMapper.mapAll(ozelliks, OzellikResponseDto.class);
    }

    @Override
    public List<OzellikResponseDto> getAllOzellik() {
        List<Ozellik> ozelliks = ozellikRepository.findAll();
        if(ozelliks.isEmpty()){
            throw new RecordNotFoundException("Ozellikler Bulunamadı.");
        }
        return USMapper.mapAll(ozelliks, OzellikResponseDto.class);
    }
}

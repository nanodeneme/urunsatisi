package com.urunsatisi.urun.app.service.impl;

import com.urunsatisi.urun.app.entity.UrunOzellik;
import com.urunsatisi.urun.app.mapper.USMapper;
import com.urunsatisi.urun.app.repository.UrunOzellikRepository;
import com.urunsatisi.urun.app.service.OzellikService;
import com.urunsatisi.urun.app.service.UrunOzellikService;
import com.urunsatisi.urun.model.response.UrunOzellilkResponseDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UrunOzellikServiceImpl implements UrunOzellikService {

    private final UrunOzellikRepository ozellikRepository;
    private final OzellikService ozellikService;

    public UrunOzellikServiceImpl(UrunOzellikRepository ozellikRepository, OzellikService ozellikService) {
        this.ozellikRepository = ozellikRepository;
        this.ozellikService = ozellikService;
    }

    @Override
    public UrunOzellilkResponseDto saveUrunOzellik(List<UrunOzellik> urunOzellik) {
        List<UrunOzellik> saved = ozellikRepository.saveAll(USMapper.mapAll(urunOzellik, UrunOzellik.class));
        return USMapper.map(saved, UrunOzellilkResponseDto.class);
    }
}

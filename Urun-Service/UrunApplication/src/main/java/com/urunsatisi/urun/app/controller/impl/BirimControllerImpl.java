package com.urunsatisi.urun.app.controller.impl;

import com.urunsatisi.urun.app.controller.BirimController;
import com.urunsatisi.urun.app.service.BirimService;
import com.urunsatisi.urun.model.request.BirimAddDto;
import com.urunsatisi.urun.model.response.BirimResponseDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BirimControllerImpl implements BirimController {

    private final BirimService birimService;

    public BirimControllerImpl(BirimService birimService) {
        this.birimService = birimService;
    }

    @Override
    public ResponseEntity<BirimResponseDto> addBirim(BirimAddDto birimAddDto) {
        return ResponseEntity.ok(birimService.addBirim(birimAddDto));
    }

    @Override
    public ResponseEntity<List<BirimResponseDto>> getBirims() {
        return ResponseEntity.ok(birimService.getAllBirim());
    }
}

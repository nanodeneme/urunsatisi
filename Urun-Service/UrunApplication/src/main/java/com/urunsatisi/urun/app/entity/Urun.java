package com.urunsatisi.urun.app.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Urun {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String aktif;

    private String aciklama;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "urun_kategori",
            joinColumns = @JoinColumn(name = "urun_id"),
            inverseJoinColumns = @JoinColumn(name = "kategori_id")
    )
    private Set<Kategori> kategoriler;

    @ManyToOne
    @JoinColumn(name = "birim_id")
    private Birim birim;
}

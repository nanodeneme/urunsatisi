package com.urunsatisi.urun.app.controller;

import com.urunsatisi.urun.model.request.UrunRequestDto;
import com.urunsatisi.urun.model.response.UrunResponseDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

public interface UrunController {

    @GetMapping("/urun")
    ResponseEntity<UrunResponseDto> getUrunById(UrunRequestDto urunRequestDto);

    @PostMapping("/urunekle")
    ResponseEntity<UrunResponseDto> addUrun(@RequestBody @Valid UrunRequestDto urunRequestDto);

}

package com.urunsatisi.urun.app.repository;

import com.urunsatisi.urun.app.entity.Birim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BirimRepository extends JpaRepository<Birim, Integer> {
}

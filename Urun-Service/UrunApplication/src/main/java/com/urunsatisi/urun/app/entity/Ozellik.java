package com.urunsatisi.urun.app.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(
        uniqueConstraints = @UniqueConstraint(
                columnNames = {"type", "value", "kategori_id"},
                name = "IND_UNIQUE_OZELLIK"
        )
)
public class Ozellik {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(nullable = false)
    private String type;

    @Column(nullable = false)
    private String value;

    @NotNull
    @Column(name = "kategori_id")
    private Integer kategoriId;
}

package com.urunsatisi.filestorage.app.exception;

public class FileStorageRuntimeException extends RuntimeException{

    public FileStorageRuntimeException(String s){
        super(s);
    }
}

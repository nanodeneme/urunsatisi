package com.urunsatisi.urun.app.repository;

import com.urunsatisi.urun.app.entity.Kategori;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface KategoriRepository extends JpaRepository<Kategori, Integer> {

    Kategori findByAdiAndUstKategori(String adi, Integer ustKategori);

    List<Kategori> findByIdIn(Set<Integer> kategoriIds);
}

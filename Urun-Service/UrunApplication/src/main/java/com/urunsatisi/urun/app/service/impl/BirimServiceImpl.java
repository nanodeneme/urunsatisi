package com.urunsatisi.urun.app.service.impl;

import com.urunsatisi.urun.app.entity.Birim;
import com.urunsatisi.urun.app.exception.RecordNotFoundException;
import com.urunsatisi.urun.app.mapper.USMapper;
import com.urunsatisi.urun.app.repository.BirimRepository;
import com.urunsatisi.urun.app.service.BirimService;
import com.urunsatisi.urun.model.request.BirimAddDto;
import com.urunsatisi.urun.model.response.BirimResponseDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BirimServiceImpl implements BirimService {

    private final BirimRepository birimRepository;

    public BirimServiceImpl(BirimRepository birimRepository) {
        this.birimRepository = birimRepository;
    }

    @Override
    public BirimResponseDto addBirim(BirimAddDto birimAddDto) {
        Birim birim = USMapper.map(birimAddDto, Birim.class);
        Birim savedBirim = birimRepository.save(birim);
        return USMapper.map(savedBirim, BirimResponseDto.class);
    }

    @Override
    public List<BirimResponseDto> getAllBirim() {
        List<Birim> birims = birimRepository.findAll();
        if(birims.isEmpty()){
            throw new RecordNotFoundException("Birimler bulunamadı.");
        }
        return USMapper.mapAll(birims, BirimResponseDto.class);
    }

    @Override
    public BirimResponseDto getBirimById(Integer id) {
        Birim birim = birimRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("Belirtilen Id ile birim bulunamadı."));
        return USMapper.map(birim, BirimResponseDto.class);
    }
}

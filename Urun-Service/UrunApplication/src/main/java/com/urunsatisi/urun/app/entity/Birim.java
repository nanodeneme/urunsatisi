package com.urunsatisi.urun.app.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Birim {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String birimAdi;

    private String aciklama;

    @OneToMany(mappedBy = "birim")
    private List<Urun> uruns;

}

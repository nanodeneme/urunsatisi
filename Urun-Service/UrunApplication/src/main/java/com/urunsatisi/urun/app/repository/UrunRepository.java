package com.urunsatisi.urun.app.repository;

import com.urunsatisi.urun.app.entity.Urun;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UrunRepository extends JpaRepository<Urun, Long> {
}

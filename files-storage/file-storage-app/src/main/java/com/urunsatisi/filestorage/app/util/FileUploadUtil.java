package com.urunsatisi.filestorage.app.util;

import com.urunsatisi.filestorage.app.exception.FileStorageRuntimeException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Component
public class FileUploadUtil {

    private static final Path PATH = Path.of("/filestorage");
    private static final Set<String> allowedMimeTypes = new HashSet<>();

    private FileUploadUtil() {
        allowedMimeTypes.add("image/jpeg");
        allowedMimeTypes.add("image/gif");
        allowedMimeTypes.add("image/png");
        allowedMimeTypes.add("image/svg+xml");
        allowedMimeTypes.add("image/tiff");

        Path path = Path.of(PATH.toUri());
        if(!Files.exists(path)){
            try {
                Files.createDirectory(path);
            } catch (IOException e) {
                throw new FileStorageRuntimeException(e.getMessage());
            }
        }
    }

    public static String saveFile(MultipartFile multipartFile){
        MultipartFile file = Objects.requireNonNull(multipartFile, "Dosya Göndermediniz.");
        if(!allowedMimeTypes.contains(file.getContentType())){
            throw new FileStorageRuntimeException("Desteklenmeyen Dosya Türü");
        }

        UUID uuid = UUID.randomUUID();
        String fileName = uuid + "." + StringUtils.getFilenameExtension(file.getOriginalFilename());
        try {
            Files.copy(file.getInputStream(), PATH.resolve(fileName));
            return fileName;
        } catch (IOException e) {
            throw new FileStorageRuntimeException(e.getMessage());
        }
    }

    public static byte[] getFile(String fileName){
        if(fileName.isEmpty()){
            throw new FileStorageRuntimeException("Dosya Adı Boş Olamaz.");
        }

        Path path = Paths.get(PATH + "/" + fileName);
        File file = new File(path.toAbsolutePath().toString());
        if(!file.exists()){
            throw new FileStorageRuntimeException("Belirtmiş Olduğunuz Dosya Bulunamadı.");
        }

        try(FileInputStream fileInputStream  = new FileInputStream(file)) {
            byte[] image = new byte[(int)file.length()];
            int byteRead = 0;
            while(byteRead < image.length){
                int bytesRemaining = image.length - byteRead;
                int bytesReadThisTime = fileInputStream.read(image, byteRead, bytesRemaining);
                if(bytesReadThisTime == -1){
                    break;
                }
                byteRead += bytesReadThisTime;
            }
            if (byteRead < image.length){
                throw new FileStorageRuntimeException("Dosya Okuma Hatası");
            }
            return image;
        } catch (IOException e) {
            throw new FileStorageRuntimeException(e.getMessage());
        }
    }

}
